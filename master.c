#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>


enum state {
	idle, want_in, in_cs
};

typedef struct SharedMemory{
	char stringList[100][256];
	int numberOfStrings;
	int turn;	
	enum state flag[19];
} SharedMemory;

// Prototypes
void destroySharedMemory();
void interruptHandler(int);

// Global variables that need to be accessed in interrupt handler
int* PID;
SharedMemory* dataPointer;
int structId;
int runningProcesses = 1;

int main (int argc, char *argv[]) {
	int hflag = 0;
	int options;
	char* fileName = "strings.in";
	PID = malloc(19 * sizeof(int));
	char errorString[100];
	unsigned int timeout = 60;
	SharedMemory data;
	dataPointer = &data;
	int structKey = 1234;
	int numberOfStrings = 0;

	// Check for any options passed with the executable and respond accordingly	
	while ((options = getopt (argc, argv, "ht:")) != -1){
		switch (options){
			case 'h':
				hflag = 1;
				break;
			case 'f':
				fileName = optarg;
				break;
			case 't':
				timeout = atoi(optarg);
				break;
			case '?':
				// If the flag wasn't recognized, print a list of available options
        			if (isprint (optopt))
					// If the flag passed was a number, the user was probably trying to pass a negative argument
					if(isdigit(optopt)){
						errno = EINVAL;
						sprintf(errorString, "%s: Error: Negative argument", argv[0]);
						perror(errorString);
						fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);

          				} else {
						fprintf (stdout, "Available options\n-- \'h\' (view arguments).\n-- \'f\' (filename)\n-- \'t\' (timer)", optopt);
              				}
				return 1;
			default:
				abort();
		}
	}
	// If the hflag is detected, print help message and exit
	if(hflag){
		fprintf(stdout, "%s has two optional arguments.\nUse -f followed by a full filename to specify a filename.\nUse -t followed by a positive integer to specify a timeout limit in seconds.\n", argv[0]);
		return 0;
	}
	if(argc > 5) {
		errno = E2BIG;
		sprintf(errorString, "%s: Error", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	}

	alarm(timeout);

        if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
		perror(errorString);
	        exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	// Create and attach shared memory for struct so that it can be accessed by all processes
	structId = shmget(structKey, sizeof(data), IPC_CREAT | 0666);
	if(structId == -1){
		sprintf(errorString, "%s: Error: Failed to allocate shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}
	dataPointer = shmat(structId, NULL, 0);
	if(dataPointer == (SharedMemory*)-1){
		sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}

	// Fill array of child process PIDs with 0's to initiate it, set turn to default, and set all child processes to idle
	int j = 0;
	for(j = 0; j < 19; j++){
		PID[j] = -1;
		dataPointer->flag[j] = idle;
	}
	dataPointer->turn = 0;
	

	// Open file and check that this was successful
	FILE* inputFile = fopen(fileName, "r");
	if(inputFile == NULL){
		sprintf(errorString, "%s: Error opening file",argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}

	// Read input from file and store in shared memory array
	char discardNewlines[5];
	int i = 0;
	while(i < 100 && (fscanf(inputFile, "%[^\n]", &dataPointer->stringList[i]) != EOF)){
		fscanf(inputFile, "\n", discardNewlines);
		numberOfStrings++;
		i++;
	}
	// Set the number of strings in the file, so that palin knows where to stop. Close file
	dataPointer->numberOfStrings = numberOfStrings;
	fclose(inputFile);

	int stringIndex = 0;
	int k, m;
	int status;
	int pid;
	for(k = 0; k < 19; k++){
		runningProcesses++;
		if((PID[k] = fork()) == -1){
		// -1 means the fork failed, handle the error
			sprintf(errorString, "%s: Error: Forking error", argv[0]);
			perror(errorString);
			destroySharedMemory();
			exit(1);
		} else if(PID[k] == 0){
		// 0 means this is the child, convert integers to strings and call palin with exec
			char processIDChar[5];
			char stringIndexChar[5];
			sprintf(processIDChar, "%d", k);
			sprintf(stringIndexChar, "%d", stringIndex);
			char* args[] = {"./palin", processIDChar, stringIndexChar};
			
			execvp("./palin", args);
	
			// If this block is reached there was an error with exec
			sprintf(errorString, "master: Exec error within child %d", (k + 1));
			perror(errorString);
			destroySharedMemory();
			exit(1);
		}
		// If neither of the two conditions above were met, it's the parent, which starts the next iteration
		stringIndex += 5;
		if(stringIndex > dataPointer->numberOfStrings) break;
	}

	// Wait for all child processes to end and clear them from the PID array as they do
	while(runningProcesses > 0){
		pid = wait(&status);
		for(m = 0; m < 19; m++){
			if(PID[m] == pid){
				PID[m] = -1;	
			}
		}
		runningProcesses--;
	}

	// Detach and remove shared memory and deallocate dynamic PID array
	destroySharedMemory();
	free(PID);
	return 0;
}

void interruptHandler(int SIGNAL){
	int i = 0;
	// Check which signal was receieved and print appropriate message
	if(SIGNAL == SIGINT){
		fprintf(stderr, "Ctrl-C interrupt detected. Terminating all processes and deallocating shared memory...\n");
	} else if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timer expired. Terminating all processes and deallocating shared memory...\n");
	}
	if(SIGNAL == SIGALRM || SIGNAL == SIGABRT){
		// Kill all child processes
		for(i = 0; i < 19; i++){
			if(PID[i] != -1){
				if (kill(PID[i], SIGNAL) == 0){
					runningProcesses--;
				}	
			}
		}
	}
	// Sleep briefly so that memory destruction message prints last
	sleep(1);
	// Detach and remove shared memory
	destroySharedMemory();
	exit(1);
}

void destroySharedMemory(){
	fprintf(stdout, "Destroying shared memory...\n");
	shmdt(dataPointer);
	shmctl(structId, IPC_RMID, NULL);
}

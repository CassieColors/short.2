EXECS = master palin

all: $(EXECS)

P1Q123: master.c
	gcc -o master master.c

P1Q4: palin.c
	gcc -o palin palin.c

clean:
	rm -f *.out
	rm -f $(EXECS)

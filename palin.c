#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>


enum state {
 idle, want_in, in_cs
};

typedef struct SharedMemory{
	char stringList[100][256];
	int numberOfStrings;
	int turn;
	enum state flag[19];
} SharedMemory;

// Prototypes
void findTime();
int isPalindrome(char*);
void interruptHandler(int);

// Global variable for handling interrupts
SharedMemory* dataPointer;
int processNumber = 0;
time_t currentTime;
struct tm* timeInfo;
char errorString[100];

int main (int argc, char *argv[]) {

	if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
                perror(errorString);
		exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	SharedMemory data;
	dataPointer = &data;
	int structKey = 1234;
	int structId;
	processNumber = atoi(argv[1]);
	int stringIndex = atoi(argv[2]);

	// Retrieve id for shared memory block and attach to it
	structId = shmget(structKey, sizeof(data), IPC_CREAT | 0666);
        if(structId == -1){
                sprintf(errorString, "%s: Error: Failed to locate shared memory", argv[0]);
                perror(errorString);
                exit(1);
        }
        dataPointer = shmat(structId, NULL, 0);
        if(dataPointer == (SharedMemory*)-1){
                sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
                perror(errorString);
                exit(1);
        }

	// If the index of the string array somehow got past the end, quit	
	if(stringIndex >= dataPointer->numberOfStrings){
		exit(1);
	}
	int numberOfProcesses = 19;
	int j, k;
	for(k = 0; k < 5; k++){
		// Find time to print into status statements
		findTime();
		if(stringIndex >= dataPointer->numberOfStrings){
                	fprintf(stderr, "%02d:%02d:%02d\tProcess %d with PID %ld finished computation.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid());
			exit(0);
		}

		// Wait for other processes to be finished with their turns and then prepare to set this process' turn to active
		do{
			findTime();
			fprintf(stderr, "%02d:%02d:%02d\tProcess %d with PID %ld wants in.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid());
			dataPointer->flag[processNumber] = want_in;
			j = dataPointer->turn;
			while(j != processNumber){
				j = ( dataPointer->flag[j] != idle ) ? dataPointer->turn : (j + 1) % numberOfProcesses;
			}		
	
			dataPointer->flag[processNumber] = in_cs;
			// Make sure no other process is in its critical section
			for(j = 0; j < numberOfProcesses; j++){
				if((j != processNumber) && (dataPointer->flag[j] == in_cs)) 
					break;
			}
		} while((j < numberOfProcesses) | (dataPointer->turn != processNumber && dataPointer->flag[dataPointer->turn] != idle));
			findTime();
			fprintf(stderr, "%02d:%02d:%02d\tProcess %d with PID %ld entered its critical section.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid());
			dataPointer->turn = processNumber;

			// Critical section
			// Build string to write into file, including process number, PID, and string
			char* output = malloc(256 * sizeof(char));
			sprintf(output, "%ld %d %s\n", (long)getpid(), (processNumber + 1), dataPointer->stringList[stringIndex]);
			
			// Wait a random amount of time (between 0 and 2 seconds)
			srand((unsigned)time(NULL));
			int random = (rand() % 3);
			sleep(random);

			// Write the string at the current index to the correct file and print what it wrote
			if(isPalindrome(dataPointer->stringList[stringIndex])){
				FILE* palinFile = fopen("palin.out", "a");
				if(fputs(output, palinFile) < 0){
					sprintf(errorString, "%s:%d: Error writing to palin.out\n", argv[0], argv[1]);
					perror(errorString);
					exit(1);
				}
				findTime();
				fprintf(stderr, "%02d:%02d:%02d\tProcess %d wrote '%ld %d %s' to file palin.out.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid(), (processNumber + 1), dataPointer->stringList[stringIndex]);
				fclose(palinFile);
			} else {
				FILE* nonPalinFile = fopen("nopalin.out", "a");
				if(fputs(output, nonPalinFile) < 0){
					sprintf(errorString, "%s:%d: Error writing to nopalin.out\n", argv[0], argv[1]);
					perror(errorString);
					exit(1);
				}
				findTime();
				fprintf(stderr, "%02d:%02d:%02d\tProcess %d wrote '%ld %d %s' to file nopalin.out.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid(), (processNumber + 1), dataPointer->stringList[stringIndex]);
				fclose(nonPalinFile);
			}
		
			// Increment the string index for this processes next iteration
			stringIndex++;

			srand((unsigned)time(NULL));
			random = (rand() % 3);
			sleep(random);

			findTime();
			fprintf(stderr, "%02d:%02d:%02d\tProcess %d with PID %ld is exiting its critical section.\n", (timeInfo->tm_hour % 12), timeInfo->tm_min, timeInfo->tm_sec, (processNumber + 1), (long)getpid());
			
			// Check if the folliwng processes are idle,find the first one that isn't and set it to their turn
			j = (dataPointer->turn + 1) % numberOfProcesses;
			while(dataPointer->flag[j] == idle){
				j = (j + 1) % numberOfProcesses;
			}

			dataPointer->turn = j;
			dataPointer->flag[processNumber] = idle;
	}
	shmdt(dataPointer);
	return 0;
}

// Compare equivalent indices of string to check if it's a palindrome
int isPalindrome(char* line){
	int isPalindrome = 1;
	int length = strlen(line);
	int startIndex = 0;
	int endIndex = (length - 1);
	while(startIndex < endIndex){
		if(line[startIndex] != line[endIndex]){
			isPalindrome = 0;
			break;
		}
		startIndex++;
		endIndex--;
	}
	return isPalindrome;
}

void findTime(){
	time(&currentTime);
	timeInfo = localtime(&currentTime);
}

// Deliver message based on interrupt and detach memory
void interruptHandler(int SIGNAL){
	if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timeout alert received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	} else {
		fprintf(stderr, "Interrupt received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	}
	shmdt(dataPointer);
	exit(1);
}
